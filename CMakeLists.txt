cmake_minimum_required(VERSION 3.12)

project(VTKmUsersGuide CXX C)

include(UseLATEX.cmake)

set(images
  images/TitleImage.png
  images/SandiaLogo.pdf
  images/DOELogo.pdf
  images/KitwareLogo.png
  images/CCBy40.svg
  images/CMakeGUIBlank.png
  images/CMakeGUI.png
  images/ArrayHandleStorage.pdf
  images/BasicRendering.png
  images/CameraViewRange2D.pdf
  images/CameraPositionOrientation.pdf
  images/CameraMovement.pdf
  images/CellConnectionsHexahedron.pdf
  images/CellConnectionsLine.pdf
  images/CellConnectionsPolyLine.pdf
  images/CellConnectionsPolygon.pdf
  images/CellConnectionsPyramid.pdf
  images/CellConnectionsQuadrilateral.pdf
  images/CellConnectionsTetrahedron.pdf
  images/CellConnectionsTriangle.pdf
  images/CellConnectionsVertex.pdf
  images/CellConnectionsWedge.pdf
  images/CellConstituents.pdf
  images/Dragon01.pdf
  images/Dragon02.pdf
  images/Dragon03.pdf
  images/Dragon04.pdf
  images/Dragon12.pdf
  images/DuplicateEdges.pdf
  images/EdgeRendering.png
  images/ExplicitCellConnections.pdf
  images/ExtrudedCellSet.pdf
  images/Hilbert01.pdf
  images/Hilbert02.pdf
  images/Hilbert03.pdf
  images/Hilbert06.pdf
  images/ImplicitBox.png
  images/ImplicitCylinder.png
  images/ImplicitFrustum.png
  images/ImplicitPlane.png
  images/ImplicitSphere.png
  images/Koch1.pdf
  images/Koch2.pdf
  images/Koch5.pdf
  images/KochApply.pdf
  images/KochParametric.pdf
  images/MCCompareCuda.png
  images/MCComparePiston.png
  images/MCCompareVTKm.png
  images/PackageHierarchy.pdf
  images/PointIncidentAngles.pdf
  images/PointRendering.png
  images/QuadraticType2_1.pdf
  images/QuadraticType2_2.pdf
  images/QuadraticType2_4.pdf
  images/ReduceByKeys.pdf
  images/StructuredCellSet.pdf
  images/Tree01.pdf
  images/Tree02.pdf
  images/Tree08.pdf
  images/VTKmEnvironments.pdf
  )

set(input_docs
  Body.tex
  Macros.tex
  LanguageDefinitions.tex
  TitlePage.tex
  Contributors.tex
  Introduction.tex
  BuildAndInstall.tex
  QuickStart.tex
  BaseTypes.tex
  Version.tex
  Initialization.tex
  DataSet.tex
  IO.tex
  RunningFilters.tex
  Rendering.tex
  ErrorHandling.tex
  ManagingDevices.tex
  Timer.tex
  ImplicitFunctions.tex
  GeneralApproach.tex
  BasicArrayHandles.tex
  SimpleWorklets.tex
  BasicFilterImpl.tex
  AdvancedTypes.tex
  Logging.tex
  WorkletTypes.tex
  FilterTypes.tex
  WorkletErrorHandling.tex
  Math.tex
  WorkingWithCells.tex
  FancyArrayHandles.tex
  AccessingAllocatingArrays.tex
  Globals.tex
  ExecutionObjects.tex
  Locators.tex
  WorkletInOut.tex
  GeneratingCellSets.tex
  VariantArrayHandle.tex
  DeviceAlgorithms.tex
  Testing.tex
  # Policies.tex
  VirtualObjects.tex
  # OpenGLInteroperability.tex
  CustomArrayStorage.tex
  TryExecute.tex
  ImplementingDeviceAdapters.tex
  FunctionInterface.tex
  WorkletArguments.tex
  NewWorkletTypes.tex
  CellShapeFigure.tex
  # Special inputs that go into verbatim-like arguments
  examples/VTKmQuickStart.cmake
  )

option(BUILD_EXAMPLES
  "When on, the examples used in the document are compiled. The examples can be run with ctest, which will report an error if they do not run correctly."
  OFF
  )

if (BUILD_EXAMPLES)
  enable_testing()
  include(CTest)
endif ()

set(VTKm_GUIDE_VERSION 1.5)

add_subdirectory(examples)

set(add_latex_document_args
  INPUTS ${input_docs}
  CONFIGURE TitlePage.tex
  IMAGES ${images}
  IMAGE_DIRS images/ColorTables
  DEPENDS ExtractExamples ${example_listings}
  USE_INDEX
  )

add_latex_document(VTKmUsersGuide.tex
  ${add_latex_document_args}
  )

add_latex_document(GettingStarted.tex
  ${add_latex_document_args}
  EXCLUDE_FROM_ALL
  )

add_latex_document(Using.tex
  ${add_latex_document_args}
  EXCLUDE_FROM_ALL
  )

add_latex_document(Developing.tex
  ${add_latex_document_args}
  EXCLUDE_FROM_ALL
  )

add_latex_document(Advanced.tex
  ${add_latex_document_args}
  EXCLUDE_FROM_ALL
  )

add_latex_document(Core.tex
  ${add_latex_document_args}
  EXCLUDE_FROM_ALL
  )
