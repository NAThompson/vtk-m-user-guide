% -*- latex -*-

\chapter{Implicit Functions}
\label{chap:ImplicitFunctions}

\index{implicit functions|(}
\index{functions!implicit|(}

\VTKm's implicit functions are objects that are constructed with values representing 3D spatial coordinates that often describe a shape.
Each implicit function is typically defined by the surface formed where the value of the function is equal to 0.
All \vtkm{ImplicitFunction}s implement \classmember*{ImplicitFunction}{Value} and \classmember*{ImplicitFunction}{Gradient} methods that describe the orientation of a provided point with respect to the \vtkm{ImplicitFunction}'s shape.

\begin{description}
\item[\classmember*{ImplicitFunction}{Value}]
  The Value method for a \vtkm{ImplicitFunction} takes a \vtkm{Vec3f} and returns a \vtkm{FloatDefault} representing the orientation of the point with respect to the \vtkm{ImplicitFunction}'s shape.
  Negative scalar values represent vector points inside of the \vtkm{ImplicitFunction}'s shape.
  Positive scalar values represent vector points outside the \vtkm{ImplicitFunction}'s shape.
  Zero values represent vector points that lie on the surface of the \vtkm{ImplicitFunction}

\item[\classmember*{ImplicitFunction}{Gradient}]
  The Gradient method for a \vtkm{ImplicitFunction} takes a \vtkm{Vec3f} and returns a \vtkm{Vec3f} representing the pointing direction from the \vtkm{ImplicitFunction}'s shape.
  Gradient calculations are more object shape specific.
  It is advised to look at the individual shape implementations for specific implicit functions.
\end{description}

Implicit functions are useful when trying to clip regions from a dataset.
For example, it is possible to use \vtkmfilter{ClipWithImplicitFunction} to remove a region in a provided dataset according to the shape of an implicit function.
See Section\ref{sec:ClipWithImplicitFunction} for more information on clipping with implicit functions.

\section{Provided Implicit Functions}

\VTKm has implementations of various implicit functions provided by the following subclasses.

\subsection{Plane}
\label{sec:Plane}

\index{implicit functions!plane|(}
\index{plane|(}

\vtkm{Plane} defines an infinite plane.
The plane is defined by a pair of \vtkm{Vec3f} values that represent the origin, which is any point on the plane, and a normal, which is a vector that is tangent to the plane.
These are set with the \classmember*{Plane}{SetOrigin} and \classmember*{Plane}{SetNormal} methods, respectively.
Planes extend infinitely from the origin point in the direction perpendicular form the Normal.
An example \vtkm{Plane} is shown in Figure~\ref{fig:ImplicitPlane}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=2.5in]{images/ImplicitPlane.png}
  \caption{
    Visual Representation of an Implicit Plane.
    The red dot and arrow represent the origin and normal of the plane, respectively.
    For demonstrative purposes the plane as shown with limited area, but in actuality the plane extends infinitely.
  }
  \label{fig:ImplicitPlane}
\end{figure}

\index{plane|)}
\index{implicit functions!plane|)}

\subsection{Sphere}
\label{sec:Sphere}

\index{implicit functions!sphere|(}
\index{sphere|(}

\vtkm{Sphere} defines a sphere.
The \textidentifier{Sphere} is defined by a center location and a radius, which are set with the \classmember*{Sphere}{SetCenter} and \classmember*{Sphere}{SetRadius} methods, respectively.
An example \vtkm{Sphere} is shown in Figure~\ref{fig:ImplicitSphere}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=2.5in]{images/ImplicitSphere.png}
  \caption{
    Visual Representation of an Implicit Sphere.
    The red dot represents the center of the sphere.
    The radius is the length of any line (like the blue one shown here) that extends from the center in any direction to the surface.
  }
  \label{fig:ImplicitSphere}
\end{figure}

\index{sphere|)}
\index{implicit functions!sphere|)}

\subsection{Cylinder}
\label{sec:Cylinder}

\index{implicit functions!cylinder|(}
\index{cylinder|(}

\vtkm{Cylinder} defines a cylinder that extends infinitely along its axis.
The cylinder is defined with a center point, a direction of the center axis, and a radius, which are set with \classmember*{Cylinder}{SetCenter}, \classmember*{Cylinder}{SetAxis}, and \classmember*{Cylinder}{SetRadius}, respectively.
An example \vtkm{Cylinder} is shown in Figure~\ref{fig:ImplicitCylinder} with set origin, radius, and axis values.

\begin{figure}[htb]
  \centering
  \includegraphics[width=2.5in]{images/ImplicitCylinder.png}
  \caption{
    Visual Representation of an Implicit Cylinder.
    The red dot represents the center value, and the red arrow represents the vector that points in the direction of the axis.
    The radius is the length of any line (like the blue one shown here) that extends perpendicular from the axis to the surface.
  }
  \label{fig:ImplicitCylinder}
\end{figure}

\index{cylinder|)}
\index{implicit functions!cylinder|)}

\subsection{Box}
\label{sec:Box}

\index{implicit functions!box|(}
\index{box|(}

\vtkm{Box} defines an axis-aligned box.
The box is defined with a pair of \vtkm{Vec3f} values that represent the minimum point coordinates and maximum point coordinates, which are set with \classmember*{Box}{SetMinPoint} and \classmember*{Box}{SetMaxPoint}, respectively.
The \textidentifier{Box} is the shape enclosed by intersecting axis-parallel lines drawn from each point.
Alternately, the \textidentifier{Box} can be specified with a \vtkm{Bounds} object using the \classmember*{Box}{SetBounds} method.
An example \vtkm{Box} is shown in Figure~\ref{fig:ImplicitBox}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=2.5in]{images/ImplicitBox.png}
  \caption{
    Visual Representation of an Implicit Box.
    The red dots represent the minimum and maximum points.
  }
  \label{fig:ImplicitBox}
\end{figure}

\index{box|)}
\index{implicit functions!box|)}

\subsection{Frustum}
\label{sec:Frustum}

\index{implicit functions!frustum|(}
\index{frustum|(}

\vtkm{Frustum} defines a hexahedral region with potentially oblique faces.
A \textidentifier{Frustum} is typically used to define the tapered region of space visible in a perspective camera projection.
The frustum is defined by the 6 planes that make up its 6 faces.
Each plane is defined by a point and a normal vector, which are set with \classmember*{Frustum}{SetPlane} and \classmember*{Frustum}{SetNormal}, respectively.
Parameters for all 6 planes can be set at once using the  \classmember*{Frustum}{SetPlanes} and \classmember*{Frustum}{SetNormals} methods.
Alternately, the \textidentifier{Frustum} can be defined by the 8 points at the vertices of the enclosing hexahedron using the \classmember*{Frustum}{CreateFromPoints} method.
The points given to \classmember*{Frustum}{CreateFromPoints} must be in hex-cell order where the first four points are assumed to be a plane, and the last four points are assumed to be a plane.
An example \vtkm{Frustum} is shown in Figure~\ref{fig:ImplicitFrustum}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=2.5in]{images/ImplicitFrustum.png}
  \caption{
    Visual Representation of an Implicit Frustum.
    The red dots and arrows represent the points and normals defining each enclosing plane.
    The blue dots represent the 8 vertices, which can also be used to define the frustum.
  }
  \label{fig:ImplicitFrustum}
\end{figure}

\index{frustum|)}
\index{implicit functions!frustum|)}

\section{General Implicit Functions}

\index{implicit functions!general|(}

It is often the case when creating code that uses an implicit function that you do not know which implicit function will be desired.
For example, the \vtkmfilter{ClipWithImplicitFunction} filter can be used with any of the implicit functions described here (\textidentifier{Plane}, \textidentifier{Sphere}, etc.).

To handle conditions where you want to support multiple implicit functions simultaneously, \VTKm provides \vtkm{ImplicitFunctionGeneral}.
Any of the implicit functions described in this chapter can be copied to a \textidentifier{ImplicitFunctionGeneral}, which will behave like the specified function.
The following example shows shows passing a \vtkm{Sphere} to \textidentifier{ClipWithImplicitFunction}, which internally uses \textidentifier{ImplicitFunctionGeneral} to manage the implicit function types.

\vtkmlisting{Passing an implicit function to a filter.}{ImplicitFunctionGeneral.cxx}

\index{implicit functions!general|)}

\index{functions!implicit|)}
\index{implicit functions|)}
