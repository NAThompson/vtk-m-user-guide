#!/bin/bash
# This script will find all the *.h *.c *.cpp *.hpp *.cxx file in
# the directory it is run in and inline convert all the files to
# clang-format
find . -iname "*.h" -o -iname "*.c" -o -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.cxx" \
    | xargs clang-format -style=file -i -fallback-style=none

exit 0