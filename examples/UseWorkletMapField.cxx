#include <vtkm/worklet/WorkletMapField.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/VariantArrayHandle.h>

#include <vtkm/VecTraits.h>
#include <vtkm/VectorAnalysis.h>

////
//// BEGIN-EXAMPLE UseWorkletMapField.cxx
////
namespace vtkm
{
namespace worklet
{

class ComputeMagnitude : public vtkm::worklet::WorkletMapField
{
public:
  using ControlSignature = void(FieldIn inputVectors, FieldOut outputMagnitudes);
  using ExecutionSignature = _2(_1);

  using InputDomain = _1;

  template<typename T, vtkm::IdComponent Size>
  VTKM_EXEC T operator()(const vtkm::Vec<T, Size>& inVector) const
  {
    return vtkm::Magnitude(inVector);
  }
};

} // namespace worklet
} // namespace vtkm
////
//// END-EXAMPLE UseWorkletMapField.cxx
////

#include <vtkm/filter/CreateResult.h>
#include <vtkm/filter/FilterField.h>

////
//// BEGIN-EXAMPLE UseFilterField.cxx
////
namespace vtkm
{
namespace filter
{

class FieldMagnitude : public vtkm::filter::FilterField<FieldMagnitude>
{
public:
  VTKM_CONT FieldMagnitude();

  //// LABEL SupportedTypes
  using SupportedTypes = vtkm::List<vtkm::Vec2f_32,
                                    vtkm::Vec2f_64,
                                    vtkm::Vec3f_32,
                                    vtkm::Vec3f_64,
                                    vtkm::Vec4f_32,
                                    vtkm::Vec4f_64>;

  template<typename ArrayHandleType, typename Policy>
  VTKM_CONT vtkm::cont::DataSet DoExecute(
    const vtkm::cont::DataSet& inDataSet,
    const ArrayHandleType& inField,
    const vtkm::filter::FieldMetadata& fieldMetadata,
    vtkm::filter::PolicyBase<Policy>);
};

} // namespace filter
} // namespace vtkm
////
//// END-EXAMPLE UseFilterField.cxx
////

////
//// BEGIN-EXAMPLE FilterFieldImpl.cxx
////
namespace vtkm
{
namespace filter
{

VTKM_CONT
FieldMagnitude::FieldMagnitude()
{
  this->SetOutputFieldName("");
}

template<typename ArrayHandleType, typename Policy>
VTKM_CONT vtkm::cont::DataSet FieldMagnitude::DoExecute(
  const vtkm::cont::DataSet& inDataSet,
  const ArrayHandleType& inField,
  const vtkm::filter::FieldMetadata& fieldMetadata,
  vtkm::filter::PolicyBase<Policy>)
{
  VTKM_IS_ARRAY_HANDLE(ArrayHandleType);

  using ComponentType =
    typename vtkm::VecTraits<typename ArrayHandleType::ValueType>::ComponentType;

  vtkm::cont::ArrayHandle<ComponentType> outField;

  this->Invoke(vtkm::worklet::ComputeMagnitude{}, inField, outField);

  std::string outFieldName = this->GetOutputFieldName();
  if (outFieldName == "")
  {
    outFieldName = fieldMetadata.GetName() + "_magnitude";
  }

  return vtkm::filter::CreateResult(
    inDataSet, outField, outFieldName, fieldMetadata);
}

} // namespace filter
} // namespace vtkm
////
//// END-EXAMPLE FilterFieldImpl.cxx
////

////
//// BEGIN-EXAMPLE RandomArrayAccess.cxx
////
namespace vtkm
{
namespace worklet
{

struct ReverseArrayCopyWorklet : vtkm::worklet::WorkletMapField
{
  using ControlSignature = void(FieldIn inputArray, WholeArrayOut outputArray);
  using ExecutionSignature = void(_1, _2, WorkIndex);
  using InputDomain = _1;

  template<typename InputType, typename OutputArrayPortalType>
  VTKM_EXEC void operator()(const InputType& inputValue,
                            const OutputArrayPortalType& outputArrayPortal,
                            vtkm::Id workIndex) const
  {
    vtkm::Id outIndex = outputArrayPortal.GetNumberOfValues() - workIndex - 1;
    if (outIndex >= 0)
    {
      outputArrayPortal.Set(outIndex, inputValue);
    }
    else
    {
      this->RaiseError("Output array not sized correctly.");
    }
  }
};

} // namespace worklet
} // namespace vtkm
////
//// END-EXAMPLE RandomArrayAccess.cxx
////

#include <vtkm/cont/testing/Testing.h>

namespace
{

void Test()
{
  static const vtkm::Id ARRAY_SIZE = 10;

  vtkm::cont::ArrayHandle<vtkm::Vec3f> inputArray;
  inputArray.Allocate(ARRAY_SIZE);
  SetPortal(inputArray.WritePortal());

  vtkm::cont::ArrayHandle<vtkm::FloatDefault> outputArray;

  vtkm::cont::DataSet inputDataSet;
  vtkm::cont::CellSetStructured<1> cellSet;
  cellSet.SetPointDimensions(ARRAY_SIZE);
  inputDataSet.SetCellSet(cellSet);
  inputDataSet.AddPointField("test_values", inputArray);

  vtkm::filter::FieldMagnitude fieldMagFilter;
  fieldMagFilter.SetActiveField("test_values");
  vtkm::cont::DataSet magResult = fieldMagFilter.Execute(inputDataSet);
  magResult.GetField("test_values_magnitude").GetData().AsArrayHandle(outputArray);

  VTKM_TEST_ASSERT(outputArray.GetNumberOfValues() == ARRAY_SIZE,
                   "Bad output array size.");
  for (vtkm::Id index = 0; index < ARRAY_SIZE; index++)
  {
    vtkm::Vec3f testValue = TestValue(index, vtkm::Vec3f());
    vtkm::Float64 expectedValue = sqrt(vtkm::Dot(testValue, testValue));
    vtkm::Float64 gotValue = outputArray.ReadPortal().Get(index);
    VTKM_TEST_ASSERT(test_equal(expectedValue, gotValue), "Got bad value.");
  }

  vtkm::cont::ArrayHandle<vtkm::Vec3f> outputArray2;
  outputArray2.Allocate(ARRAY_SIZE);

  vtkm::cont::Invoker invoker;
  invoker(vtkm::worklet::ReverseArrayCopyWorklet{}, inputArray, outputArray2);

  VTKM_TEST_ASSERT(outputArray2.GetNumberOfValues() == ARRAY_SIZE,
                   "Bad output array size.");
  for (vtkm::Id index = 0; index < ARRAY_SIZE; index++)
  {
    vtkm::Vec3f expectedValue = TestValue(ARRAY_SIZE - index - 1, vtkm::Vec3f());
    vtkm::Vec3f gotValue = outputArray2.ReadPortal().Get(index);
    VTKM_TEST_ASSERT(test_equal(expectedValue, gotValue), "Got bad value.");
  }
}

} // anonymous namespace

int UseWorkletMapField(int argc, char* argv[])
{
  return vtkm::cont::testing::Testing::Run(Test, argc, argv);
}
